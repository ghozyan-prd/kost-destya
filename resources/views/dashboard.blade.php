
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="icon" type="image/png" href="imgs/logo1.png" width="512px " height="512px" >
        <title>Home | KOST-DESTYA</title>

		<!-- Meta Description -->
        <meta name="description" content="Blue One Page Creative HTML5 Template">
        <meta name="keywords" content="one page, single page, onepage, responsive, parallax, creative, business, html5, css3, css3 animation">
        <meta name="author" content="Muhammad Morshed">

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="css/main.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="js/modernizr-2.6.2.min.js"></script>

    </head>

    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
                    </button>
					<!-- /responsive nav button -->

					<!-- logo -->
					<h1 class="navbar-brand">
						<a href='{{ route('login') }}'>' KOST DESTYA '</a>
					</h1>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="#body">Home</a></li>
                        <li><a href="#service">Service</a></li>
                        <li><a href="#portfolio">portfolio</a></li>
                        <li><a href="#testimonials">Testimonial</a></li>
                        <li><a href="#price">price</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>

				<!-- /main nav -->

            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->

		<main class="site-content" role="main">

        <!--
        Home Slider
        ==================================== -->

		<section id="home-slider">
            <div id="slider" class="sl-slider-wrapper">

				<div class="sl-slider">

					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

						<div class="bg-img bg-img-1"></div>

						<div class="slide-caption">
                            <div class="caption-content">
                                <h2 class="animated fadeInDown">KOST - DESTYA</h2>
                                <li class="animated fadeInDown">Penting bagi kami memberikan rasa aman & nyaman bagi seluruh penghuni, agar tetap terasa seperti di rumah sendiri <br> Lokasi yang dekat dengan Universitas Negeri Surabaya dan Telkom Regional Surabaya <br> membuat kami menjadi pilihan utama kost bagi para Mahasiswi UNESA  </li>

                            </div>
                        </div>

					</div>

					<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">

						<div class="bg-img bg-img-2"></div>
						<div class="slide-caption">
              <div class="caption-content">
                  <h2 class="animated fadeInDown">KOST - DESTYA</h2>
                  <li class="animated fadeInDown">Penting bagi kami memberikan rasa aman & nyaman bagi seluruh penghuni, agar tetap terasa seperti di rumah sendiri <br> Lokasi yang dekat dengan Universitas Negeri Surabaya dan Telkom Regional Surabaya <br> membuat kami menjadi pilihan utama kost bagi para Mahasiswi UNESA  </li>

              </div>
                        </div>

					</div>

					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">

						<div class="bg-img bg-img-3"></div>
						<div class="slide-caption">
              <div class="caption-content">
                  <h2 class="animated fadeInDown">KOST - DESTYA</h2>
                  <li class="animated fadeInDown">Penting bagi kami memberikan rasa aman & nyaman bagi seluruh penghuni, agar tetap terasa seperti di rumah sendiri <br> Lokasi yang dekat dengan Universitas Negeri Surabaya dan Telkom Regional Surabaya <br> membuat kami menjadi pilihan utama kost bagi para Mahasiswi UNESA  </li>

              </div>
                        </div>

					</div>

				</div><!-- /sl-slider -->

                <!--
                <nav id="nav-arrows" class="nav-arrows">
                    <span class="nav-arrow-prev">Previous</span>
                    <span class="nav-arrow-next">Next</span>
                </nav>
                -->

                <nav id="nav-arrows" class="nav-arrows hidden-xs hidden-sm visible-md visible-lg">
                    <a href="javascript:;" class="sl-prev">
                        <i class="fa fa-angle-left fa-3x"></i>
                    </a>
                    <a href="javascript:;" class="sl-next">
                        <i class="fa fa-angle-right fa-3x"></i>
                    </a>
                </nav>


				<nav id="nav-dots" class="nav-dots visible-xs visible-sm hidden-md hidden-lg">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
				</nav>

			</div><!-- /slider-wrapper -->
		</section>

        <!--
        End Home SliderEnd
        ==================================== -->

			<!-- about section -->
			<section id="about" >
				<div class="container">
					<div class="row">
						<div class="col-md-4 wow animated fadeInLeft">
							<div class="recent-works">
								<h3>Indekos (KOST)</h3>
								<div id="works">
									<div class="work-item">
										<p> sebuah jasa yang menawarkan sebuah kamar atau tempat untuk ditinggali dengan sejumlah pembayaran tertentu untuk setiap periode tertentu (umumnya pembayaran per bulan).<br> <br>Kata ini diserap dari frasa bahasa Belanda "in de kost". Definisi "in de kost" sebenarnya adalah "makan di dalam", tetapi dapat pula berarti "tinggal dan ikut makan" di dalam rumah tempat menumpang tinggal.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-7 col-md-offset-1 wow animated fadeInRight">
							<div class="welcome-block">
								<h3>Welcome To My Site</h3>
						     	 <div class="message-body">
									<img src="img/ozy.jpg" class="pull-left" alt="member">
						       		<p>Semoga pemanfaatan teknologi ini yang bertujuan membantu Owner Kost Destya dalam menggunakan manajemen keuangan ini dapat menjadikan Kost destya semakin berkembang. Karena Untuk menunjang keberhasilan, proses pelayanan yang dilakukan harus dengan maksimal, serta akses teknologi yang memadai <br> <br> Developed <br><a href="http://ozy-dev.web.id/">Ghozyan Hilman Pradana</a></p>
                   </div>

						    </div>
						</div>
					</div>
				</div>
			</section>
			<!-- end about section -->


			<!-- Service section -->
			<section id="service">
				<div class="container">
					<div class="row">

						<div class="sec-title text-center">
							<h2 class="wow animated bounceInLeft">Why Choose Us</h2>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-home fa-3x"></i>
								</div>
								<h3>NYAMAN</h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.3s">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-tasks fa-3x"></i>
								</div>
								<h3>AMAN</h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.6s">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-clock-o fa-3x"></i>
								</div>
								<h3>BERSIH</h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.9s">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-heart fa-3x"></i>
								</div>

								<h3>TERPECAYA</h3>
							</div>
						</div>

					</div>
				</div>
			</section>
			<!-- end Service section -->

			<!-- portfolio section -->
			<section id="portfolio">
				<div class="container">
					<div class="row">

						<div class="sec-title text-center wow animated fadeInDown">
							<h2>Picture Of Kost-Destya</h2>
							<p>Sebagian Foto Kost </p>
						</div>


						<ul class="project-wrapper wow animated fadeInUp">
							<li class="portfolio-item">
								<img src="img/portfolio/work1.jpg" class="img-responsive" alt="Lokasi Jl Ketintang Timur PTT III No 32 Surabaya, Indonesia">
								<figcaption class="mask">
									<h3>Tampak Depan</h3>
									<p>Lokasi Jl Ketintang Timur PTT III No 32 Surabaya, Indonesia</p>
								</figcaption>
								<ul class="external">
									<li><a class="fancybox" title="Tampak Depan" data-fancybox-group="works" href="img/portfolio/work1.jpg"><i class="fa fa-search"></i></a></li>
								</ul>
							</li>

							<li class="portfolio-item">
								<img src="img/portfolio/work3.jpg" class="img-responsive" alt="Berada di ruang tengah, lantai 2 Kost Destya">
								<figcaption class="mask">
									<h3>Ruang Santai</h3>
									<p>Berada di ruang tengah, lantai 2 Kost Destya</p>
								</figcaption>
								<ul class="external">
									<li><a class="fancybox" title="Ruang Santai" href="img/portfolio/work3.jpg" data-fancybox-group="works" ><i class="fa fa-search"></i></a></li>

								</ul>
							</li>

							<li class="portfolio-item">
								<img src="img/portfolio/work5.jpg" class="img-responsive" alt="Kamar lantai 2 Kost Destya">
								<figcaption class="mask">
									<h3>Kamar Lantai 2</h3>
									<p>Kamar lantai 2 Kost Destya</p>
								</figcaption>
								<ul class="external">
									<li><a class="fancybox" title="Kamar Lantai 2" data-fancybox-group="works" href="img/portfolio/work5.jpg"><i class="fa fa-search"></i></a></li>

								</ul>
							</li>

							<li class="portfolio-item">
								<img src="img/portfolio/work4.jpg" class="img-responsive" alt="Kamar lantai 3 Kost Destya">
								<figcaption class="mask">
									<h3>Kamar Lantai 3</h3>
									<p>Kamar lantai 3 Kost Destya</p>
								</figcaption>
								<ul class="external">
									<li><a class="fancybox" title="Kamar Lantai 3" data-fancybox-group="works" href="img/portfolio/work4.jpg"><i class="fa fa-search"></i></a></li>

								</ul>
							</li>

							<li class="portfolio-item">
								<img src="img/portfolio/work6.jpg" class="img-responsive" alt="Dapur boleh digunakan <br> Khusus Makanan Ringan dan Minuman Ringan">
								<figcaption class="mask">
									<h3>Dapur</h3>
									<p>Dapur boleh digunakan <br> Khusus Makanan Ringan dan Minuman Ringan</p>
								</figcaption>
								<ul class="external">
									<li><a class="fancybox" title="Dapur" data-fancybox-group="works" href="img/portfolio/work6.jpg"><i class="fa fa-search"></i></a></li>

								</ul>
							</li>

							<li class="portfolio-item">
								<img src="img/portfolio/work7.jpg" class="img-responsive" alt="Parkiran menampung sekitar 15 Motor">
								<figcaption class="mask">
									<h3>Parkiran</h3>
									<p>Parkiran menampung sekitar 15 Motor</p>
								</figcaption>
								<ul class="external">
									<li><a class="fancybox" title="Parkiran" data-fancybox-group="works" href="img/portfolio/work7.jpg"><i class="fa fa-search"></i></a></li>

								</ul>
							</li>
						</ul>

					</div>
				</div>
			</section>
			<!-- end portfolio section -->

			<!-- Testimonial section -->
			<section id="testimonials" class="parallax">
				<div class="overlay">
					<div class="container">
						<div class="row">

							<div class="sec-title text-center white wow animated fadeInDown">
								<h2>What people say</h2>
                <p>Testimoni</p>
							</div>

							<div id="testimonial" class=" wow animated fadeInUp">
								<div class="testimonial-item text-center">
									<img src="img/hijab.jpg" alt="Our Clients">
									<div class="clearfix">
										<span>Ayu Desi Dianasari</span>
										<p>Kost-Destya nyaman dan aman. Fasilitasnya bagus dan bersih. Pemilik kostnya juga sangat ramah dan baik. Saya senang di Kost-Destya karena lingkungan / kondisi kost sangat cozy, compatible dan tidak ada hal-hal yang mengganggu sehingga membuat saya nyaman</p>
									</div>
								</div>
								<div class="testimonial-item text-center">
									<img src="img/hijab3.jpg" alt="Our Clients">
									<div class="clearfix">
										<span>Niendya Afriza</span>
										<p>Pengalaman saya selama tinggal di Kost-Destya adalah mereka sangat menjaga kebersihan, kenyamanan dan keamanan penghuninya. Selain itu pemiliknya juga ramah dan perhatian, sehingga saya yang tinggal jauh dari orang tua merasa memiliki keluarga disini dan saya selalu dibantu ketika membutuhkan bantuan apapun</p>
									</div>
								</div>
								<div class="testimonial-item text-center">
									<img src="img/hijab2.jpg" alt="Our Clients">
									<div class="clearfix">
										<span>Tika Nadifatul</span>
										<p>Kost-Destya nyaman, aman dan harganya bersahabat hehe.... Posisinya juga strategis.. dekat universitas, rumah sakit, mall, masjid, dll...</p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
			<!-- end Testimonial section -->

			<!-- Price section -->
			<section id="price">
				<div class="container">
					<div class="row">

						<div class="sec-title text-center wow animated fadeInDown">
							<h2>Price</h2>
							<p>Everyone's Pricing Table</p>
						</div>

						<div class="col-md-4 wow animated fadeInUp">
							<div class="price-table featured text-center">
								<span>Tipe A</span>
								<div class="value">
									<span>Rp</span>
									<span>650K</span><br>
									<span>month/person</span>
								</div>
								<ul>
									<li>Free Listri, Air & Almari</li>
									<li>1 Kamar di isi 2 orang</li>
									<li>Tempat Tidur + Bantal</li>
									<li>Wifi 24 Jam</li>
									<li><a href="">Penuh</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.4s">
							<div class="price-table featured text-center">
								<span>Tipe B</span>
								<div class="value">
									<span>Rp</span>
									<span>800K</span><br>
									<span>month/person</span>
								</div>
								<ul>
                  <li>Free Listri, Air & Almari</li>
									<li>Sekamar Sendiri</li>
									<li>Tempat Tidur + Bantal</li>
									<li>Wifi 24 Jam</li>
									<li><a href="">Penuh</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.8s">
							<div class="price-table featured text-center">
								<span>Tipe C</span>
								<div class="value">
									<span>Rp</span>
									<span>1,1Jt</span><br>
									<span>month/person</span>
								</div>
								<ul>
                  <li>Free Listri, Air & Almari</li>
									<li>1 Kamar di isi 2 orang</li>
									<li>Tempat Tidur + Bantal</li>
									<li>Wifi 24 Jam + AC</li>
									<li><a href="">Penuh</a></li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</section>
			<!-- end Price section -->

			<!-- Social section -->
			<section id="social" class="parallax">
				<div class="overlay">
					<div class="container">
						<div class="row">

							<div class="sec-title text-center white wow animated fadeInDown">
								<h2>FOLLOW US</h2>
								<p>Beautifully simple follow buttons to help you get followers on</p>
							</div>

							<ul class="social-button">
								<li class="wow animated zoomIn"><a href="https://www.facebook.com/Ghozyan"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="https://www.instagram.com/kost.destya/?hl=id"><i class="fa fa-instagram fa-2x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="mailto:kost.destya@gmail.com"><i class="fa fa-envelope fa-2x"></i></a></li>
							</ul>


						</div>
					</div>
				</div>
			</section>
			<!-- end Social section -->

			<!-- Contact section -->
			<section id="contact" >
				<div class="container">
					<div class="row">

						<div class="sec-title text-center wow animated fadeInDown">
							<h2>Contact</h2>
							<p>Leave a Message</p>
						</div>


						<div class="col-md-7 contact-form wow animated fadeInLeft">
							<form action="#" method="post">
								<div class="input-field">
									<input type="text" name="name" class="form-control" placeholder="Your Name...">
								</div>
								<div class="input-field">
									<input type="email" name="email" class="form-control" placeholder="Your Email...">
								</div>
								<div class="input-field">
									<input type="text" name="subject" class="form-control" placeholder="Subject...">
								</div>
								<div class="input-field">
									<textarea name="message" class="form-control" placeholder="Messages..."></textarea>
								</div>

							</form>
						</div>

						<div class="col-md-5 wow animated fadeInRight">
							<address class="contact-details">
								<h3>Contact Us</h3>
								<p><i class="fa fa-pencil"></i>PT Kost Destya<span>POS 62321</span> <span>Jl Ketintang Timur PTT III No 32, Surabaya </span><span>Indonesia</span></p><br>
								<p><i class="fa fa-phone"></i>Phone: (+6281)273-338-544 </p>
								<p><i class="fa fa-envelope"></i>kost.destya@gmail.com</p>
							</address>
						</div>

					</div>
				</div>
			</section>
			<!-- end Contact section -->

			<section id="google-map">
				<div id="map-canvas" class="wow animated fadeInUp"></div>
			</section>

		</main>

		<footer id="footer">
			<div class="container">
				<div class="row text-center">
					<div class="footer-content">
						<div class="wow animated fadeInDown">
						</div>
            <img src="imgs/logo.png" width="200px" height="200px" class="img-circle" alt="User Image">

						<form action="#" method="post" class="subscribe-form wow animated fadeInUp">
							<div class="input-field">
							</div>
						</form>
						<div class="footer-social">
							<ul>
								<li class="wow animated zoomIn"><a href="mailto:ghozyan.ozy@my.gavilan.edu.com"><i class="fa fa-envelope fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="https://twitter.com/ghozyan_prd?s=08"><i class="fa fa-twitter fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="https://www.instagram.com/ghozyan_prd/?hl=id"><i class="fa fa-instagram fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="1.2s"><a href="https://www.youtube.com/channel/UCCWEtBYU4PlP1qpDOU5nT_g?view_as=subscriber"><i class="fa fa-youtube fa-3x"></i></a></li>
							</ul>
						</div>

						<p>Copyright &copy; 2019 Design and Developed <br> By <a href="http://ozy-dev.web.id/">Ghozyan Hilman Pradana</a> </p>
					</div>
				</div>
			</div>
		</footer>

		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="js/jquery.slitslider.js"></script>
        <script src="js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="js/main.js"></script>
    </body>
</html>
